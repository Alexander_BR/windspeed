public class WindVelocity
{
private int kmh;

public WindVelocity(int kmh)
{
    this.kmh = kmh;
}
public boolean isOrkan()
{
    return this.kmh > 120;
}
public boolean isWindLess()
{
    return this.kmh < 2;
}
public double meterPerSeconds()
{
return this.kmh / 3.6;
}
public double seaMilesperHour()
{
    return this.kmh / 1.852;
}
public double beaufort()
{
    int beaufort = (int) (Math.pow(meterPerSeconds()/ 3.01, 0.6666) + 0.5);
    if (beaufort > 12)
        beaufort = 12;
    return beaufort;
}


}
